import { Todo.WebPage } from './app.po';

describe('todo.web App', () => {
  let page: Todo.WebPage;

  beforeEach(() => {
    page = new Todo.WebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
