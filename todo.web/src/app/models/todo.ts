export class Todo {
  name: String;
  description: String;
  when: Date;
  status: String;
}
