import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AccessComponent } from './components/access/access.component';
import { AuthGuard } from './guards';

const appRoutes: Routes = [
  { path: '', component: DashboardComponent, canActivate: [ AuthGuard ] },
  { path: 'access', component: AccessComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
