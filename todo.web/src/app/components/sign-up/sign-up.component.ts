import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { AuthenticationService } from '../../services/index';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  model: any = {};

  constructor(
    private authenticationService: AuthenticationService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authenticationService.register(this.model.email, this.model.name,
      this.model.password, this.model.password_confirmation)
      .subscribe(
        data => {
          this.toastr.success('Now you have to login');
        },
        data => {
          console.log(data);
          for(let error of data.json().errors) {
            this.toastr.error(error);
          }
        });
  }


}
