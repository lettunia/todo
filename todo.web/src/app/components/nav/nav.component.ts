import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  me: any;

  constructor() {
    NavComponent.updateUserStatus.subscribe(res => {
      this.me = JSON.parse(sessionStorage.getItem('currentUser'));
    })
  }

  ngOnInit() {
    this.me = JSON.parse(sessionStorage.getItem('currentUser'));
  }

  public static updateUserStatus: Subject<boolean> = new Subject();

  signOut() {
    sessionStorage.removeItem('currentUser');
  }

}
