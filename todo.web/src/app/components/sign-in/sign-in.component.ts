import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NavComponent } from '../nav/nav.component';
import { AuthenticationService } from '../../services';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  model: any = {};

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authenticationService.login(this.model.email, this.model.password)
      .subscribe(
        data => {
          this.router.navigate(['**']);
          NavComponent.updateUserStatus.next(true);
          this.toastr.success('Welcome');
        },
        data => {
          console.log(data);
          for(let error of data.json().errors) {
            this.toastr.error(error);
          }
      });
  }

}
