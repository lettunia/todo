import { Component, OnInit } from '@angular/core';

import { TodoService } from '../../services';
import { Todo } from '../../models';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todos: Todo[] = [];

  constructor(
    private todoService: TodoService
  ) { }

  ngOnInit() {
    this.todoService.getAll().subscribe(todos => { this.todos = todos; });
  }

}
