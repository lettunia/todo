import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { Todo } from '../../models';
import { TodoService } from "../../services";

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.css']
})
export class TodoFormComponent implements OnInit {
  todo: Todo = new Todo();

  constructor(
    private todoService: TodoService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  onSubmit() {
    this.todo.status = 'new';
    this.todoService.create(this.todo)
      .subscribe(
        data => {
          this.toastr.success('Todo created');
          location.reload();
        },
        data => {
          console.log(data);
          for(let error of data.json().errors) {
            this.toastr.error(error);
          }
        });
  }
}
