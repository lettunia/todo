import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }

  login(email: String, password: String) {
    return this.http.post('/api/sign_in', JSON.stringify({ email, password }))
      .map((response: Response) => {
        let user = response.json();
        if (user && user.token) {
          sessionStorage.setItem('currentUser', JSON.stringify(user));
        }
      });
  }

  register(email: String, name: String,
           password: String, password_confirmation: String) {

    return this.http.post('/api/sign_up',
      JSON.stringify({ email, password, password_confirmation, name })
    );
  }

  logout() {
    sessionStorage.removeItem('currentUser');
  }

}
