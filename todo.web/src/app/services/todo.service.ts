import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Todo } from '../models';
import { Observable } from "rxjs/Observable";

@Injectable()
export class TodoService {
  private todosUrl = '/api/todos';

  constructor(private http: Http) { }

  getAll(): Observable<Todo[]> {
    return this.http.get(this.todosUrl)
      .map((response: Response) => response.json());
  }

  create(todo: Todo): Observable<Todo> {
    return this.http.post(this.todosUrl, JSON.stringify(todo))
      .map((response: Response) => response.json());
  }
}
