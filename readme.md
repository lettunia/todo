# Configuración del backend

### En la carpeta `todo.backend`

```bash
composer install
```

## Crear base de datos

Si vas a usar Postgresql es necesario crear la base de datos usando los comandos de Postgres.

```
createdb database_name
```

Realizar una copia de `.env.example` y nombrarlo `.env`, posteriormente agregar las variables de configuración de base de datos.


```
DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=todo
DB_USERNAME=user
DB_PASSWORD=pass
```

luego correr las migraciones

```
php artisan migrate
```

# Correr el servidor

```
php artisan serve
```

# Link a  la colección de postman con todos los endpoints del backend 

[Todo Api Postman](Todo API.postman_collection)


# Configuración del frontend

## cli necesarios


```
npm install -g @angular/cli
```

### En la carpeta `todo.web`

Ejecutar el comando `yarn` si se tiene yarn instalado, si no, `npm install`


### Configuración de variables de entorno

Crear el directorio `environments` con el archivo `environment.ts` 

```
export const environment = {
  production: false,
  api: 'http://localhost:8000'
};
```

> si cambió el puerto del backend agregar el puerto correcto en api.

# Correr el frontend

```
ng serve --open
```