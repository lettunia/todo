<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Response;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('tymon.jwt.absent', function () {
            return Response::jsonError(['Token absent']);
        });

        Event::listen('tymon.jwt.expired', function () {
            return Response::jsonError(['Token expired']);
        });

        Event::listen('tymon.jwt.invalid', function () {
            return Response::jsonError(['Token invalid']);
        });

        Event::listen('tymon.jwt.user_not_found', function () {
            return Response::jsonError(['User not found']);
        });
    }
}
