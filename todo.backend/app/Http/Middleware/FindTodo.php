<?php

namespace App\Http\Middleware;

use App\Todo;
use Closure;

class FindTodo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $todo = Todo::where('id', $request->route('todo'))->first();

        if (!$todo) {
            return response()->jsonError(['Todo not found']);
        }

        $request->merge(compact('todo'));

        return $next($request);
    }
}
