<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BaseRequest extends FormRequest {

    protected function formatErrors(Validator $validator) {
        return ['errors' => array_flatten($validator->errors()->toArray())];
    }
}
