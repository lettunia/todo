<?php

namespace App\Http\Controllers\Api;

use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\SignInRequest;
use App\Http\Controllers\Controller;

class AuthController extends Controller {

    public function signUp(SignUpRequest $request) {

        $input = $request->only(['name', 'email', 'password', 'password_confirmation']);

        return User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
        ]);
    }

    public function signIn(SignInRequest $request) {

        $input = $request->only(['email', 'password']);

        try {
            if (!$token = JWTAuth::attempt($input)) {
                return response()->jsonError(['Invalid credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->jsonError(['Could not create token'], 500);
        }

        return response()->json(array_merge(JWTAuth::toUser($token)->toArray(), ['token' => $token]));
    }

}
