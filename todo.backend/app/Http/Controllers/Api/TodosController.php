<?php

namespace App\Http\Controllers\Api;

use App\Http\Middleware\FindTodo;
use Illuminate\Http\Request;
use App\Http\Requests\TodoRequest;
use App\Http\Controllers\Controller;

class TodosController extends Controller {

    public function __construct() {
        $this->middleware('jwt.auth');

        $this->middleware(FindTodo::class)
            ->only(['update', 'destroy', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        return $request->user()->todos;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request) {
        return $request->user()->todos()
            ->create($request->only(['name', 'description', 'when', 'status']));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        return $request->todo;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TodoRequest $request) {
        $request->todo->fill($request->only(['name', 'description', 'when', 'status']));
        $request->todo->save();
        return $request->todo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $request->todo->delete();
        return $request->todo;
    }
}
